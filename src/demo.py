import torch



x = torch.Tensor([0.1, 1, 3])

print(torch.linalg.norm(x, dim=0, ord=1))
print(torch.linalg.norm(x, dim=0, ord=2))
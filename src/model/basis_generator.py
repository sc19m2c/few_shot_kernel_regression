import torch 


class BasisGenerator(torch.nn.Module):
    
    def __init__(self, input_dims, num_layers, basis_dims):
        super().__init__()
        layers = []
        for layer in range(num_layers-1):
            layers.append(torch.nn.Linear(input_dims if layer == 0 else basis_dims, basis_dims))
            layers.append(torch.nn.ReLU())
            
        layers.append(torch.nn.Linear(basis_dims, basis_dims))
        # layers.append(torch.nn.ReLU())


        def init_weights(m):
            if isinstance(m, torch.nn.Linear):
                torch.nn.init.xavier_uniform(m.weight)
                m.bias.data.fill_(0.01)
        
        self.network = torch.nn.Sequential(*layers)
        self.network.apply(init_weights)
        
        return

    def forward(self, x):

      
        return self.network(x)



    
    
    

    
    
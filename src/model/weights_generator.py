import torch
import torch.nn as nn
import torch.nn.functional as F
import math

class WeightsGenerator(torch.nn.Module):
    
    def __init__(self, input_dims, embedding_dims, attention_layers, output_dims):

        super().__init__()

        self.input_projection = nn.Linear(input_dims, embedding_dims, bias=True)
        self.output_projection = nn.Linear(embedding_dims, output_dims, bias=True)

        attn_layers = []
        for i in range(attention_layers):
            attn_layers.append(SelfAttention(embedding_dims))

        self.network = nn.Sequential(
            self.input_projection,
            *attn_layers,
            self.output_projection
        )

        torch.nn.init.xavier_uniform(self.input_projection.weight)
        self.input_projection.bias.data.fill_(0.01)
        torch.nn.init.xavier_uniform(self.output_projection.weight)
        self.output_projection.bias.data.fill_(0.01)
        
        return
    

    def forward(self, x):
        return torch.mean(self.network(x), 1)
    
    
    
class SelfAttention(torch.nn.Module):
    
    def __init__(self, embedding_dims, attn_dropout = 0.2):

        super().__init__()

        self.embedding_dims = embedding_dims
        self.attention_dropout = attn_dropout

        self.layer_norm1 = torch.nn.LayerNorm(normalized_shape=embedding_dims, eps=1e-5, elementwise_affine=True)
        self.layer_norm2 = torch.nn.LayerNorm(normalized_shape=embedding_dims, eps=1e-5, elementwise_affine=True)

        self.fc1 = nn.Linear(self.embedding_dims, self.embedding_dims*2, bias=True)
        self.fc2 = nn.Linear(self.embedding_dims*2, self.embedding_dims, bias=True)

        self.ff = torch.nn.Sequential(
            self.fc1,
            torch.nn.ReLU(),
            nn.Dropout(p=attn_dropout), 
            self.fc2,
            nn.Dropout(p=attn_dropout))

        self.k_proj = nn.Linear(self.embedding_dims, self.embedding_dims, bias=True)
        self.v_proj = nn.Linear(self.embedding_dims, self.embedding_dims, bias=True)
        self.q_proj = nn.Linear(self.embedding_dims, self.embedding_dims, bias=True)

        # Xavier initialisation
        torch.nn.init.xavier_uniform(self.fc1.weight)
        torch.nn.init.xavier_uniform(self.fc2.weight)
        self.fc1.bias.data.fill_(0.01)
        self.fc2.bias.data.fill_(0.01)
        nn.init.xavier_uniform_(self.k_proj.weight, gain=1 / math.sqrt(2))
        nn.init.xavier_uniform_(self.v_proj.weight, gain=1 / math.sqrt(2))
        nn.init.xavier_uniform_(self.q_proj.weight, gain=1 / math.sqrt(2))
        nn.init.xavier_uniform_(self.q_proj.weight, gain=1 / math.sqrt(2))

        return


    def forward(self, x):
        
        residual = x.clone()
        #get key query value projections
        #k,q,v .size = [batch_size, sample_size, hidden_dims]
        K = self.k_proj(x)
        Q = self.q_proj(x)
        V = self.v_proj(x)

        K = torch.transpose(K, 1, 2) #[batch_size, hidden_dims, sample_size] for bmm attn_score calc
        attn_score = torch.bmm(Q,K) # [batch_size, sample_size, sample_size ]
        attn_score /= self.embedding_dims #scaling

        W = torch.softmax(attn_score, dim=2) #convert the attention score into prob distribution of attention weights : # [batch_size, sample_size, sample_size ]
        W = torch.dropout(W,self.attention_dropout, self.training) # dropout some weights for regularization

        attn = torch.bmm(W,V) #[batch_size, sample_size, hidden_dims ]
        attn = torch.dropout(attn, p=self.attention_dropout, train=self.training)

        state = attn + residual # the residual connection [batch_size, sample_size, hidden_dims ]
        state = self.layer_norm1(state)
        residual = state.clone()

        state = self.ff(state)
        state += residual
        state = self.layer_norm2(state)

        return state
    

class MultiHeadAttention:
    
    #IDEA ---- not sure if neccessary
    
    def __init__(self):
        return
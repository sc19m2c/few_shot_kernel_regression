
from model.basis_generator import BasisGenerator
from model.weights_generator import WeightsGenerator
from dataLoader.dataloader import FewshotData

import torch
from torch.utils.data import DataLoader
from tqdm import tqdm
from torch.optim import Adam
from torch.utils.data.dataloader import default_collate

from collections import OrderedDict

class KernelLearner(torch.nn.Module):
    
    def __init__(self, input_dims, num_layers, basis_dims, embedding_dims, attention_layers, prediction_dims, l1, l2, k_shot, **kwargs):

        super(KernelLearner, self).__init__()

        self.train_data = FewshotData(k_shot, mode="train")
        self.test_data = FewshotData(k_shot, mode="test")

        self.l1 = l1
        self.l2 = l2
        self.config = kwargs
        self.k_shot = k_shot
        
        
        self.known_basis_generator = BasisGenerator(input_dims, num_layers, basis_dims)
        self.prediction_basis_generator = BasisGenerator(input_dims, num_layers, basis_dims)
        #IDEA test to see if this is really required
        self.weights_generator = WeightsGenerator(basis_dims+prediction_dims, embedding_dims, attention_layers, basis_dims)

        self.known_basis_generator.to(self.config['device'])
        self.prediction_basis_generator.to(self.config['device'])
        self.weights_generator.to(self.config['device'])

        params = list(self.known_basis_generator.parameters()) + list(self.prediction_basis_generator.parameters()) + list(self.weights_generator.parameters())
        self.network_opt = Adam(params, lr=kwargs["learning_rate"], weight_decay=kwargs['l2_weight_decay'])

        #CHECK this could cause differences between my model and theirs
        self.lr_scheduler = torch.optim.lr_scheduler.ExponentialLR(optimizer=self.network_opt, gamma=kwargs["lr_decay"])
        
        

        self.loss = torch.nn.MSELoss()

        #TODO needs to ensure training always set here
        
        return


    def forward(self, x_known, y_known, x_prediction):

        #[batch_size, sample_size, basis_dims]
        known_features = self.known_basis_generator(x_known)
        prediction_features = self.prediction_basis_generator(x_prediction)
        known_embedding = torch.cat((known_features, y_known), 2) # [batch_size, sample_size, basis_dims + prediction_dims]

        weights = self.weights_generator(known_embedding) #[batch, basis_dims]
        weights = weights.unsqueeze(1) # [batch, 1, basis_dims]
        weights = torch.transpose(weights, 1, 2) # for bmm [batch, basis_dims, 1]

        
        predictions = torch.bmm(prediction_features, weights) # [batch_size, sample_size, 1] -> assuming that prediction dims == 1

        return predictions, weights.squeeze(2)


    def calc_loss(self, predictions, targets, weights, prediction_sample_size ):

 

        pen_loss = self.l1 * torch.linalg.norm(weights, ord=1) + \
                    self.l2 * torch.linalg.norm(weights, ord=2)


        predictions = torch.reshape(predictions, (self.config['batch_size']*prediction_sample_size, 1)).squeeze(1)
        targets = torch.reshape(targets, (self.config['batch_size']*prediction_sample_size, 1)).squeeze(1)


        return self.loss(predictions, targets) + pen_loss



    def train_model(self):

        #iterations
        #epochs
        #data loader
        
        self.test_model()


        for epoch in range(self.config['epochs']):

            epoch_loss = 0

            self.train()

            loader = DataLoader(
                self.train_data,
                batch_size=self.config["batch_size"],
                num_workers=1,
                # shuffle=True
                # collate_fn=lambda x: tuple(x_.to(self.config["device"]) for x_ in default_collate(x))
            )

            stats = OrderedDict()
            stats['loss'] = 0
            progress_bar = tqdm(loader, desc='| Epoch {:03d}'.format(1), leave=False, disable=False)

            for i, (x_known, y_known, x_pred, y_pred) in enumerate(progress_bar):

                x_known, y_known, x_pred, y_pred = x_known.to(self.config["device"]), y_known.to(self.config["device"]), x_pred.to(self.config["device"]), y_pred.to(self.config["device"])

          

                prediction, weights = self.forward(x_known.float(), y_known.float(), x_pred.float())


                loss = self.calc_loss(prediction, y_pred.float(), weights, self.k_shot)



                epoch_loss += loss.cpu().item() 
                stats['loss'] += loss.cpu().item() 

                loss.backward()
                self.network_opt.step()
                self.network_opt.zero_grad()

                progress_bar.set_postfix({key: '{:.4g}'.format(value / (i + 1)) for key, value in stats.items()},
                                     refresh=True)



            self.lr_scheduler.step()
            print("average loss === ", epoch_loss/(240000/self.config["batch_size"]))

            self.test_model()


        return 



    def test_model(self):

        self.eval()

        assert self.known_basis_generator.training == False
        assert self.prediction_basis_generator.training == False
        assert self.weights_generator.training == False


        epoch_loss = 0

        loader = DataLoader(
            self.test_data,
            batch_size=self.config["batch_size"],
            num_workers=1,
            # shuffle=True
            # collate_fn=lambda x: tuple(x_.to(self.config["device"]) for x_ in default_collate(x))
        )

        stats = OrderedDict()
        stats['loss'] = 0
        progress_bar = tqdm(loader, desc='| validation {:03d}'.format(1), leave=False, disable=False)

        for i, (x_known, y_known, x_pred, y_pred) in enumerate(progress_bar):

            x_known, y_known, x_pred, y_pred = x_known.to(self.config["device"]), y_known.to(self.config["device"]), x_pred.to(self.config["device"]), y_pred.to(self.config["device"])

            prediction, weights = self.forward(x_known.float(), y_known.float(), x_pred.float())

           
            
            loss = self.calc_loss(prediction, y_pred.float(), weights, 100)

            epoch_loss += loss.cpu().item() 

            progress_bar.set_postfix({key: '{:.4g}'.format(value / (i + 1)) for key, value in stats.items()},
                                    refresh=True)

     
        print("test loss === ", epoch_loss/(self.test_data.__len__()/self.config["batch_size"]))

        return 
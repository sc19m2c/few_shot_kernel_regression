from dataLoader.dataloader import FewshotData
from model.kernel_learner import KernelLearner

import torch
from torch.utils.data import DataLoader
from tqdm import tqdm

config ={
    "num_tasks":240000,
    "num_tasks_test":100,
    "seed":157,
    "l1":1e-5,
    "l2":1e-4,
    "epochs":500,
    "k_shot":10,
    "num_layers":3,
    "basis_dims":40,
    "embedding_dims":64,
    "attention_layers":3,
    "input_dims":1,
    "prediction_dims":1,
    "batch_size":10,

    "l2_weight_decay":1e-6,
    "learning_rate":5e-4,
    "lr_decay":0.5,

    "device": torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
}



kernel = KernelLearner(**config)
kernel.train_model()

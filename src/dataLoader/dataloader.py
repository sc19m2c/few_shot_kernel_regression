import numpy as np
import os
import random
import pickle

from torch.utils.data import Dataset




class FewshotData(Dataset):

    def __init__(self, k_shot, mode ="train"):

        self.k_shot = k_shot
        # self.bmaml = bmaml
        

        with open(f'./dataLoader/sinusoidal_{mode}.pkl', 'rb') as f:
            data = pickle.load(f)
            self.data = dict()
            self.data['train_x'] = data['x'][:, :self.k_shot, :]
            self.data['train_y'] = data['y'][:, :self.k_shot, :]
            self.data['test_x'] = data['x'][:, 20:, :]
            self.data['test_y'] = data['y'][:, 20:, :]

        self.dim_input = 1
        self.dim_output = 1

        print('load data: train_x', self.data['train_x'].shape, 'test_x', self.data['test_x'].shape, 'train_y',
              self.data['train_y'].shape, 'test_y', self.data['test_y'].shape)


        return


    def __getitem__(self, indx):
        context_x = self.data['train_x'][indx]
        context_y = self.data['train_y'][indx]
        target_x = self.data['test_x'][indx]
        target_y = self.data['test_y'][indx]
  
        return context_x, context_y, target_x, target_y


    def __len__(self):
        'Denotes the total number of samples'
        return len(self.data['train_x'])
# few_shot_kernel_regression


- implimentation of : FEW-SHOT REGRESSION VIA LEARNING SPARSIFYING BASIS FUNCTIONS 
- note: this implimentation achieves greater performance due to error in the original implimentation of self attention.
- original source code : https://github.com/fewshotreg/Few-Shot-Regression



# Moving forward 

- experiment 1: does the model actually require seperate test and train feature extractors.
- experiment 2: does the model rely on the use of Y as a feature. 
- experiment 3: can we modify the model for zero-shot learning
>> - idea: use bert masking rules for the inputs Y - therefore we dont need any known Y's to regress for unseen tasks. 
- experiment 4: best methods for higher dimensional regression tasks.
